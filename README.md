# OpenID Connect

This crate provides async and sync implementations of an OpenID Connect Client as specified by the
[OpenID Core 1.0 specification.](https://openid.net/specs/openid-connect-core-1_0.html). It also implements 
[OpenID Connect Discovery 1.0](https://openid.net/specs/openid-connect-discovery-1_0.html) to take advantage
of OpenID providers that make configuration endpoints avaialble to simplify setup. Known supporters of the
canonical Discovery protocol are found under the `issuer` module.

If your application is asychronous, it is recommended to use the async API. There is a bit more
manual work involved (you will need to manually invoke token decoding and userinfo validation) but 
the process of decoding a JWT can be non-trivial when being done for a significant number of users 
logging in. The asynchronous API uses the `futures-await` crate, which constrains this library to 
`nightly` Rust for the time being.

If your application is not natively asynchronous, the `sync` module provides an encapsulated form
of the async client that also simplifies the methods required to achieve authentication. Discovery
is built into the constructor of `sync::Client`, the Token is assumed `Expiring` (because most OP
tokens are when using the auth code flow) and besides having to still provide the redirect for your
users and having your own endpoint for receiving auth_code authentication takes one step - 
`authenticate` - which returns the complete validated and decoded Token and the userinfo if 
available.