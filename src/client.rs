use biscuit::{self, Empty, errors};
use biscuit::jwa::{self, SignatureAlgorithm};
use biscuit::jwk::{AlgorithmParameters, JWKSet};
use biscuit::jws::{Compact, Secret};
use chrono::{Duration, Utc};
use futures::prelude::*;
use futures::future::Future;
use hyper::{self, header, Method, Request, Uri};
use hyper_rustls::HttpsConnector;
use inth_oauth2::Client as AuthClient;
use inth_oauth2::client::ClientError;
use inth_oauth2::provider::Provider as AuthProvider;
use inth_oauth2::Token as AuthToken;
use inth_oauth2::token::Lifetime;
use serde_json::from_slice;
use url::Url;
use url_serde;
use validator::Validate;

use std;
use std::borrow::Borrow;
use std::str::FromStr;
use std::rc::Rc;

use discovery::{Config, Discovered};
use provider::Provider;
use token::{Token, Claims};

type IdToken = Compact<Claims, Empty>;

/// Optional parameters that [OpenID specifies](https://openid.net/specs/openid-connect-basic-1_0.html#RequestParameters) for the auth URI.
#[derive(Default)]
pub struct Options {
    pub(crate) nonce: Option<String>,
    display: Option<Display>,
    prompt: Option<std::collections::HashSet<Prompt>>,
    pub(crate) max_age: Option<Duration>,
    ui_locales: Option<String>,
    claims_locales: Option<String>,
    id_token_hint: Option<String>,
    login_hint: Option<String>,
    acr_values: Option<String>,
}

pub enum Display {
    Page,
    Popup,
    Touch,
    Wap,
}

impl Display {
    fn as_str(&self) -> &'static str {
        match *self {
            Display::Page => "page",
            Display::Popup => "popup",
            Display::Touch => "touch",
            Display::Wap => "wap",
        }
    }
}

#[derive(PartialEq, Eq, Hash)]
pub enum Prompt {
    None,
    Login,
    Consent,
    SelectAccount,
}

impl Prompt {
    fn as_str(&self) -> &'static str {
        match self {
            &Prompt::None => "none",
            &Prompt::Login => "login",
            &Prompt::Consent => "consent",
            &Prompt::SelectAccount => "select_account",
        }
    }
}

#[derive(Debug)]
pub enum DecodeError {
    Biscuit(errors::Error),
    Validation(ValidationError),
    MissingKid,
    MissingKey,
    EmptySet,
}

impl From<errors::Error> for DecodeError {
    fn from(e: errors::Error) -> Self {
        DecodeError::Biscuit(e)
    }
}

impl From<ValidationError> for DecodeError {
    fn from(e: ValidationError) -> Self {
        DecodeError::Validation(e)
    }
}

#[derive(Debug)]
pub enum ValidationError {
    Biscuit(errors::Error),
    Mismatch(Mismatch),
    Missing(Missing),
    Expired(Expiry),
}

impl From<errors::Error> for ValidationError {
    fn from(e: errors::Error) -> Self {
        ValidationError::Biscuit(e)
    }
}

#[derive(Debug)]
pub enum Mismatch {
    Audience,
    Authorized,
    Issuer,
    Nonce,
    Subject,
}

#[derive(Debug)]
pub enum Missing {
    AuthorizedParty,
    AuthTime,
    Nonce,
}

#[derive(Debug)]
pub enum Expiry {
    Expires,
    IssuedAt,
    MaxAge,
}

/// The userinfo struct contains all possible userinfo fields regardless of scope. [See spec.](https://openid.net/specs/openid-connect-basic-1_0.html#StandardClaims)
// TODO is there a way to use claims_supported in config to simplify this struct?
#[derive(Deserialize, Validate)]
pub struct Userinfo {
    pub sub: String,
    pub name: Option<String>,
    pub given_name: Option<String>,
    pub family_name: Option<String>,
    pub middle_name: Option<String>,
    pub nickname: Option<String>,
    pub preferred_username: Option<String>,
    #[serde(with = "url_serde")]
    pub profile: Option<Url>,
    #[serde(with = "url_serde")]
    pub picture: Option<Url>,
    #[serde(with = "url_serde")]
    pub website: Option<Url>,
    #[validate(email)]
    pub email: Option<String>,
    pub email_verified: Option<bool>,
    // Isn't required to be just male or female
    pub gender: Option<String>,
    // ISO 9601:2004 YYYY-MM-DD or YYYY. Would be nice to serialize to chrono::Date.
    pub birthdate: Option<String>,
    // Region/City codes. Should also have a more concrete serializer form.
    pub zoneinfo: Option<String>,
    // Usually RFC5646 langcode-countrycode, maybe with a _ sep, could be arbitrary
    pub locale: Option<String>,
    // Usually E.164 format number
    pub phone_number: Option<String>,
    pub phone_number_verified: Option<bool>,
    pub address: Option<Address>,
    pub updated_at: Option<i64>,
}

/// Address Claim struct. Can be only formatted, only the rest, or both.
#[derive(Deserialize)]
pub struct Address {
    pub formatted: Option<String>,
    pub street_address: Option<String>,
    pub locality: Option<String>,
    pub region: Option<String>,
    // Countries like the UK use alphanumeric postal codes, so you can't just use a number here
    pub postal_code: Option<String>,
    pub country: Option<String>,
}

/// Triplet of required details for a client of a given provider.
/// These are the client_id, client_secret, and redirect_uri of the spec.
#[derive(Serialize, Deserialize)]
pub struct Params {
    pub id: String,
    pub secret: String,
    #[serde(with = "url_serde")]
    pub url: Url,
}

// Common pattern in the Client::decode function when dealing with mismatched keys
macro_rules! wrong_key {
    ($expected:expr, $actual:expr) => (
        Err(DecodeError::from(
            errors::Error::WrongKeyType {
                expected: format!("{:?}", $expected),
                actual: format!("{:?}", $actual)
            }
        ))
    )
}

// We have to specify the bound on P here because AuthClient is also bounded.
pub struct Client<P: AuthProvider> {
    oauth: AuthClient<P>,
    jwks: JWKSet<Empty>,
    client: Rc<hyper::Client<HttpsConnector>>,
}

/// Generic Client impl for any Provider type
impl<P: AuthProvider + Provider> Client<P> {
    /// If you want to create a client using Discovery (preferred):
    ///
    /// ```
    /// use oidc::{discovery, IdcProvider, Oidc};
    /// use provider::IdcProvider;
    /// use hyper::{Uri, Client};
    /// use hyper_rustls::HttpsConnector;
    /// use std::rc::Rc;
    /// use tokio_core::reactor::Core;
    ///
    /// let mut core = Core::new()?;
    /// let handle = core.handle();
    /// let client = Client::configure()
    ///      .connector(HttpsConnector::new(4, &handle))
    ///      .build(&handle);
    ///
    /// let resource = Uri::from_str("joe@example.com").map_err(|e| hyper::Error::Uri(e))?;
    /// et host = Url::from_str("https://example.com")?;
    ///
    /// let issuer = discovery::issuer(resource, &client, Some(host))?;
    /// let config = issuer.and_then(|url| discovery::config(url, &client));
    /// // We need the config for the provider later.
    /// let config = core.run(jwks)?;
    /// let jwks_uri = Uri::from_str(config.jwks_uri.as_str()).map_err(|e| hyper::Error::Uri(e))?;
    /// // We run the event loop to get the key set to cosntruct the client.
    /// let jwks = core.run(discovery::jwks(jwks_uri, &client))?;
    ///
    /// let provider = Discovered::new(config);
    /// let client = Rc::new(client);
    /// let oidc = Oidc::new("super_awesome_webapp".to_string(),
    ///                      "a_secret_to_everybody".to_string(),
    ///                      "https://localhost/redirect".to_string(),
    ///                      provider, jwks, client);
    /// ```
    ///
    /// Otherwise, you can implement your own provider, and get the key set however you want.
    pub fn new(
        params: Params,
        provider: P,
        jwks: JWKSet<Empty>,
        client: Rc<hyper::Client<HttpsConnector>>,
    ) -> Self {
        Client {
            oauth: AuthClient::with_provider(
                params.id,
                params.secret,
                provider,
                Some(params.url.into_string()),
            ),
            jwks,
            client,
        }
    }

    /// This wraps the internal oauth's auth_uri generator.
    ///
    /// Redirect clients to this url, and when they come back to your redirect_uri.
    ///
    /// Your redirect uri destination should handle [2.1.5.1 of the implementers guide.](https://openid.net/specs/openid-connect-basic-1_0.html#CodeOK)
    ///
    /// Verify the state is the same and pass the token value into request_token.
    ///
    /// If scope doesn't contain openid we will add it for you - it is required by spec.
    /// This means if you just want a default scope, using default on scope works.
    ///
    /// State is only defined as "recommended" in the spec, but we mandate it here.
    /// Just do it, don't get CSRF'd.
    pub fn auth_url(
        &self,
        mut scope: String,
        state: &str,
        options: &Options,
    ) -> Result<Url, ClientError> {
        if scope.is_empty() {
            scope = "openid".to_string()
        } else if !scope.contains("openid") {
            scope += match scope.chars().rev().peekable().peek() {
                Some(end) => {
                    if !end.is_whitespace() {
                        "openid"
                    } else {
                        " openid"
                    }
                }
                None => unreachable!("Scope wasn't empty according to is_empty!"),
            }
        }
        let mut url = self.oauth.auth_uri(Some(&scope), Some(state))?;
        {
            let mut query = url.query_pairs_mut();
            if let Some(ref nonce) = options.nonce {
                query.append_pair("nonce", nonce.as_str());
            }
            if let Some(ref display) = options.display {
                query.append_pair("display", display.as_str());
            }
            if let Some(ref prompt) = options.prompt {
                let s = prompt.iter().map(|s| s.as_str()).collect::<Vec<_>>().join(
                    " ",
                );
                query.append_pair("prompt", s.as_str());
            }
            if let Some(max_age) = options.max_age {
                query.append_pair("max_age", max_age.num_seconds().to_string().as_str());
            }
            if let Some(ref ui_locales) = options.ui_locales {
                query.append_pair("ui_locales", ui_locales.as_str());
            }
            if let Some(ref claims_locales) = options.claims_locales {
                query.append_pair("claims_locales", claims_locales.as_str());
            }
            if let Some(ref id_token_hint) = options.id_token_hint {
                query.append_pair("id_token_hint", id_token_hint.as_str());
            }
            if let Some(ref login_hint) = options.login_hint {
                query.append_pair("login_hint", login_hint.as_str());
            }
            if let Some(ref acr_values) = options.acr_values {
                query.append_pair("acr_values", acr_values.as_str());
            }
        }
        Ok(url)
    }

    /// This wraps the internal oauth's token request.
    ///
    /// Run the future you get back from this, and it gives you a jwt token.
    pub fn request_token(
        &self,
        authorization_code: &str,
    ) -> impl Future<Item = P::Token, Error = ClientError> {
        self.oauth.request_token(
            self.client.borrow(),
            authorization_code,
        )
    }

    /// Decodes an openid id token. This consumes the encoded form and makes it decoded.
    /// This operation is complex, so once decoded you should hold on to your id token.
    /// The nonce and max_age are values optionally used in auth_uri. If they were used,
    /// provide them again here - the Client itself is stateless per request.
    ///
    /// Note - this takes the *id_token* of a complete OpenID Token. An OpenID token
    /// from the token endpoint comes in two parts - {Bearer, IdToken}. We only want
    /// the later part - the former is a standard Oauth authorization token, the later
    /// is a signed JWT whose verification authenticates a user.
    pub fn decode_token<L: Lifetime>(
        &self,
        token: &mut IdToken,
        nonce: Option<&str>,
        max_age: Option<&Duration>,
    ) -> Result<(), DecodeError> {
        // This is an early escape if the token is already decoded
        token.encoded()?;

        let header = token.unverified_header()?;
        // If there is more than one key, the token MUST have a key id
        let key = if self.jwks.keys.len() > 1 {
            let token_kid = header.registered.key_id.ok_or(DecodeError::MissingKid)?;
            self.jwks.find(&token_kid).ok_or(DecodeError::MissingKey)?
        } else {
            self.jwks.keys.first().as_ref().ok_or(DecodeError::EmptySet)?
        };

        if let Some(alg) = key.common.algorithm.as_ref() {
            if let &jwa::Algorithm::Signature(alg) = alg {
                if header.registered.algorithm != alg {
                    return wrong_key!(alg, header.registered.algorithm);
                }
            } else {
                return wrong_key!(SignatureAlgorithm::default(), alg);
            }
        }

        let alg = header.registered.algorithm;
        match key.algorithm {
            // HMAC
            AlgorithmParameters::OctectKey { ref value, .. } => {
                match alg {
                    SignatureAlgorithm::HS256 |
                    SignatureAlgorithm::HS384 |
                    SignatureAlgorithm::HS512 => {
                        *token = token.decode(&Secret::Bytes(value.clone()), alg)?;
                        self.validate_token(&token, nonce, max_age)?;
                        Ok(())
                    }
                    _ => wrong_key!("HS256 | HS384 | HS512", alg),
                }
            }
            AlgorithmParameters::RSA(ref params) => {
                match alg {
                    SignatureAlgorithm::RS256 |
                    SignatureAlgorithm::RS384 |
                    SignatureAlgorithm::RS512 => {
                        let pkcs = Secret::Pkcs {
                            n: params.n.clone(),
                            e: params.e.clone(),
                        };
                        *token = token.decode(&pkcs, alg)?;
                        self.validate_token(&token, nonce, max_age)?;
                        Ok(())
                    }
                    _ => wrong_key!("RS256 | RS384 | RS512", alg),
                }
            }
            AlgorithmParameters::EllipticCurve(_) => unimplemented!("No support for EC keys yet"),
        }
    }

    /// Validation according to the OpenID Connect Spec section [2.2.1.](https://openid.net/specs/openid-connect-basic-1_0.html#IDTokenValidation)
    ///
    /// Issuer is *required*. If you are not using Discovery, you need to get it another way.
    /// It also must be an EXACT match. Every OIDC provider will list their issuer url somewhere.
    ///
    /// Of note we do not validate acr. They are client / provider specific and would require a
    /// client defined closure to validate anyway. If you are using acr, make sure to validate it!
    pub fn validate_token(
        &self,
        token: &IdToken,
        nonce: Option<&str>,
        max_age: Option<&Duration>,
    ) -> Result<(), ValidationError> {
        let claims = token.payload()?;

        if claims.iss != *self.oauth.provider.issuer() {
            return Err(ValidationError::Mismatch(Mismatch::Issuer));
        }

        if let Some(nonce) = nonce {
            match claims.nonce {
                Some(ref test) => {
                    if test != nonce {
                        return Err(ValidationError::Mismatch(Mismatch::Nonce));
                    }
                }
                None => return Err(ValidationError::Missing(Missing::Nonce)),
            }
        }

        if !claims.aud.contains(&self.oauth.client_id) {
            return Err(ValidationError::Mismatch(Mismatch::Audience));
        }
        // By spec, if there are multiple auds, we must have an azp
        if let biscuit::SingleOrMultiple::Multiple(_) = claims.aud {
            if let None = claims.azp {
                return Err(ValidationError::Missing(Missing::AuthorizedParty));
            }
        }
        // If there is an authorized party, it must be our client_id
        if let Some(ref azp) = claims.azp {
            if azp != &self.oauth.client_id {
                return Err(ValidationError::Mismatch(Mismatch::Authorized));
            }
        }

        let now = Utc::now();
        // Now should never be less than the time this code was written!
        if now.timestamp() < 1504758600 {
            panic!("chrono::Utc::now() can never be before this was written!")
        }
        if claims.exp <= now.timestamp() {
            return Err(ValidationError::Expired(Expiry::Expires));
        }

        if let Some(age) = max_age {
            match claims.auth_time {
                Some(time) => {
                    // This is not currently be risky business. That could change.
                    if time >= (now - *age).timestamp() {
                        return Err(ValidationError::Expired(Expiry::MaxAge));
                    }
                }
                None => return Err(ValidationError::Missing(Missing::AuthTime)),
            }
        }

        Ok(())
    }

    /// Get the userinfo manually using your own userinfo url. This function validates
    /// that the userinfo origin matches the token origin (as long as you verified it!)
    pub fn userinfo_manual<L: Lifetime>(
        &self,
        token: &Token<L>,
        url: &Url,
    ) -> Result<impl Future<Item = Userinfo, Error = ClientError>, ValidationError> {
        if url.origin() != self.oauth.provider.issuer().origin() {
            return Err(ValidationError::Mismatch(Mismatch::Issuer));
        }
        let uri = Uri::from_str(url.as_str()).unwrap();
        let auth_code = token.access_token().to_string();
        Ok(userinfo_internal(Rc::clone(&self.client), uri, auth_code))
    }
}

/// Validate your userinfo responses given the IdToken Claims (this just checks sub)
// TODO This should really be a part of userinfo handling. There are two problems though:
// 1. This has to come from the future, where we are using ClientErrors and not validation ones.
// 2. We are taking the Token rather than the IdToken, and due to the limitations of Biscuit that
//    would mean we have to make sure its decoded, which isn't a part of ValidationError - it is
//    a DecodeError. That would require we escalate Userinfo into a decode function.
// Even in this function, we take the Claims from an IdToken rather than the token itself because
// we don't want to provide the custom error handling here to deal with encoded tokens.
pub fn validate_userinfo(token: &Claims, info: &Userinfo) -> Result<(), ValidationError> {
    if token.sub == info.sub {
        Ok(())
    } else {
        Err(ValidationError::Mismatch(Mismatch::Subject))
    }
}

/// Provider impl for Discovered providers that have configuration documents
impl<L: Lifetime> Client<Discovered<L>> {
    /// You can get the Discovery Configuration Serialization from a Discovered Client.
    pub fn config(&self) -> &Config {
        &self.oauth.provider.config
    }

    /// Returns Option because we might not have a userinfo in the config
    pub fn userinfo(
        &self,
        token: &Token<L>,
    ) -> Option<impl Future<Item = Userinfo, Error = ClientError>> {
        // TODO Is there some way to cut down on this boilerplate?
        match self.config().userinfo_endpoint {
            Some(ref url) => self.userinfo_manual(token, url).ok(),
            None => None,
        }
    }
}

/// Takes an owned copy of the access_token() from the actual token
#[async]
fn userinfo_internal(
    client: Rc<hyper::Client<HttpsConnector>>,
    uri: hyper::Uri,
    token: String,
) -> Result<Userinfo, ClientError> {
    let mut req: Request = hyper::Request::new(Method::Get, uri);
    // TODO Would rather use into<header> on Token, but that
    // only works on ref tokens and messes with lifetimes
    req.headers_mut().set(header::Authorization(
        header::Bearer { token },
    ));
    req.set_body(hyper::Body::empty());
    let resp = await!(client.request(req))?;
    let body = await!(resp.body().concat2())?;
    let info = from_slice::<Userinfo>(&body)?;
    Ok(info)
}
