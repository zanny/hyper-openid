//! # Synchronous OpenID Connect Client
//!
//! If you don't need the speed of async (or can't support it), this `Client` wraps the regular one
//! and does all the async calls synchronously. All functions also return a single error type for
//! simplicities sake.

use biscuit::jws::Compact;
use hyper;
use hyper_rustls::HttpsConnector;
use inth_oauth2::client::ClientError;
use inth_oauth2::token::Expiring;
use num_cpus;
use tokio_core::reactor::Core;
use url::Url;

use std::io;
use std::rc::Rc;

use client::{self, DecodeError, Options, Params, Userinfo, ValidationError};
use discovery::{discover, Discovered};
use token::Token;

pub enum Error {
    Decode(DecodeError),
    Io(io::Error),
    Oauth(ClientError),
}

impl From<DecodeError> for Error {
    fn from(e: DecodeError) -> Self {
        Error::Decode(e)
    }
}

impl From<ValidationError> for Error {
    fn from(e: ValidationError) -> Self {
        Error::Decode(DecodeError::Validation(e))
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

impl From<ClientError> for Error {
    fn from(e: ClientError) -> Self {
        Error::Oauth(e)
    }
}


pub struct Client {
    client: client::Client<Discovered<Expiring>>,
    core: Core,
}

impl Client {
    /// Construct a new client by discovering a given issuer
    pub fn new(issuer: Url, params: Params) -> Result<Self, Error> {
        let mut core = Core::new()?;
        let client = Rc::new(
            hyper::Client::configure()
                .connector(HttpsConnector::new(num_cpus::get(), &core.handle()))
                .build(&core.handle()),
        );
        let fut = discover(issuer, params, Rc::clone(&client));
        let oidc = core.run(fut)?;
        Ok(Client { client: oidc, core })
    }

    /// Get the url to send users to for authentication at the OP.
    /// Make sure the state you give here matches the request from the user!
    /// This function doesn't locally have errors - any errors are from the oauth part and will
    /// be Oauth's ClientError.
    pub fn auth_url(&self, scope: String, state: &str, options: &Options) -> Result<Url, Error> {
        self.client.auth_url(scope, state, options).map_err(|e| {
            Error::Oauth(e)
        })
    }

    /// If you supplied a nonce or max_age in the `url` getters `Options`, provide it here again.
    /// This function does everything, and will return the complete validated Token and any
    /// userinfo if avaialble.
    ///
    /// This can produce a lot of different errors:
    /// * Event loop failure emits an io::Error
    /// * Token request failure emits an Oauth ClientError
    /// * Token decoding failure emits DecodeError, which can itself be...
    ///   * An error in Biscuit, the JOSE library we use, in regards to the crypto
    ///   * A validation error since we validate in the decode() step automatically
    ///   * Any of the documented enumerated self documenting native values in DecodeError
    /// * If a userinfo endpoint exists in the config, but it doesn't share an issuer origin with
    ///   the token root, that is a Validation::Mismatch::Issuer error (and should never happen!)
    /// * If userinfo was successful the subjects must match between token and info. Otherwise
    ///   you get a Validation::Mismatch::Subject error.
    pub fn authenticate(
        &mut self,
        authorization_code: &str,
        options: &Options,
    ) -> Result<(Token<Expiring>, Option<Userinfo>), Error> {
        let fut = self.client.request_token(authorization_code);
        let mut token = self.core.run(fut)?;

        self.client.decode_token::<Expiring>(
            &mut token.id_token,
            options.nonce.as_ref().map(String::as_ref),
            options.max_age.as_ref(),
        )?;
        if let Some(ref url) = self.client.config().userinfo_endpoint {
            let fut = self.client.userinfo_manual(&token, url)?;
            let info = self.core.run(fut)?;
            if let Compact::Decoded { ref payload, .. } = token.id_token {
                client::validate_userinfo(&payload, &info)?;
            } else {
                unreachable!("The token HAS TO BE decoded at this point!")
            }
            return Ok((token, Some(info)));
        }
        Ok((token, None))
    }
}
