use url::Url;

/// OpenID Connect providers must impl both oauth::Provider and oidc::Provider.
pub trait Provider {
    /// We are required y spec to validate issuers when confirming Tokens.
    fn issuer(&self) -> &Url;
    /// The jwks_uri is required by spec, and gives us a place to get keys to verify jwts.
    fn jwks_url(&self) -> &Url;
}
