//! [OpenID Connect Discovery](https://openid.net/specs/openid-connect-discovery-1_0.html) Implementation.
//!
//! There are three primary methods that all run on a hyper client to retrieve data related to
//! discovery. The example below shows how to use all three to retrieve the json web keys (JWKS)
//! from an OpenID Connect server from just a webfinger.
//!
//! ```
//! use oidc::discovery;
//! use hyper::{Uri, Client};
//! use hyper_rustls::HttpsConnector;
//! use tokio_core::reactor::Core;
//!
//! let mut core = Core::new()?;
//! let handle = core.handle();
//! let client = Client::configure()
//!     .connector(HttpsConnector::new(4, &handle))
//!     .build(&handle);
//!
//! let resource = Uri::from_str("joe@example.com").map_err(|e| hyper::Error::Uri(e))?;
//! let host = Url::from_str("https://example.com")?;
//!
//! let issuer = discovery::issuer(resource, &client, Some(host))?;
//! let config = issuer.and_then(|url| discovery::config(url, &client));
//! let jwks = config.and_then(|config| {
//!    // We currently need to do an ugly conversion from url to uri...
//!    let uri = Uri::from_str(config.jwks_uri.as_str()).map_err(|e| hyper::Error::Uri(e))?;
//!    Ok(discovery::jwks(uri, &client))
//! });
//! let jwks = core.run(jwks)?;
//! ```

use inth_oauth2::client::ClientError;
use inth_oauth2::client::response::ParseError::ExpectedType;
use inth_oauth2::provider::Provider as AuthProv;
use inth_oauth2::token::{Expiring, Lifetime};
use biscuit::Empty;
use biscuit::jwk::JWKSet;
use futures::prelude::*;
use futures::future::Future;
use futures::stream::Stream;
use hyper::{self, Client, Uri};
use hyper_rustls::HttpsConnector;

use serde_json::from_slice;
use url::{self, Url};
use url_serde;
use validator::Validate;

use std::marker::PhantomData;
use std::str::FromStr;
use std::rc::Rc;

use client::Client as IdClient;
use client::Params;
use provider::Provider;
use token::Token;

// dead_code on subject, a part of webfinger spec we never use
#[allow(dead_code)]
#[derive(Deserialize)]
struct Webfinger {
    // Should be a Uri, but we don't use it, and Uri has no serde support
    subject: String,
    // TODO should these json arrays be Box<[T]> instead of Vec?
    links: Vec<Link>,
}

// Webfinger Links are much more complex than this, but we should only get
// links of this format back from valid openid_connect webfingers
// dead_Code on rel: String, which is a part of webfinger but we never use it
#[allow(dead_code)]
#[derive(Deserialize)]
struct Link {
    // We use a Url rel, but you can't assume it. A proper Webflinger
    // implementation should use an enum type here of Uri and RFC 5988 relation
    rel: String,
    #[serde(with = "url_serde")]
    href: Url,
}

#[derive(Deserialize)]
pub struct Config {
    #[serde(with = "url_serde")]
    pub issuer: Url,
    #[serde(with = "url_serde")]
    pub authorization_endpoint: Url,
    #[serde(with = "url_serde")]
    // Only optional in the implicit flow
    // TODO For now, we only support code flows.
    pub token_endpoint: Url,
    #[serde(with = "url_serde")]
    pub userinfo_endpoint: Option<Url>,
    #[serde(with = "url_serde")]
    pub jwks_uri: Url,
    #[serde(with = "url_serde")]
    pub registration_endpoint: Option<Url>,
    pub scopes_supported: Option<Vec<String>>,
    // There are only three valid response types, plus combinations of them, and none
    // If we want to make these user friendly we want a struct to represent all 7 types
    pub response_types_supported: Vec<String>,
    // There are only two possible values here, query and fragment. Default is both.
    pub response_modes_supported: Option<Vec<String>>,
    // Must support at least authorization_code and implicit.
    pub grant_types_supported: Option<Vec<String>>,
    pub acr_values_supported: Option<Vec<String>>,
    // pairwise and public are valid by spec, but servers can add more
    pub subject_types_supported: Vec<String>,
    // Must include at least RS256, none is only allowed with response types without id tokens
    pub id_token_signing_alg_values_supported: Vec<String>,
    pub id_token_encryption_alg_values_supported: Option<Vec<String>>,
    pub id_token_encryption_enc_values_supported: Option<Vec<String>>,
    pub userinfo_signing_alg_values_supported: Option<Vec<String>>,
    pub userinfo_encryption_alg_values_supported: Option<Vec<String>>,
    pub userinfo_encryption_enc_values_supported: Option<Vec<String>>,
    pub request_object_signing_alg_values_supported: Option<Vec<String>>,
    pub request_object_encryption_alg_values_supported: Option<Vec<String>>,
    pub request_object_encryption_enc_values_supported: Option<Vec<String>>,
    // Spec options are client_secret_post, client_secret_basic, client_secret_jwt, private_key_jwt
    // If omitted, client_secret_basic is used
    pub token_endpoint_auth_methods_supported: Option<Vec<String>>,
    // Only wanted with jwt auth methods, should have RS256, none not allowed
    pub token_endpoint_auth_signing_alg_values_supported: Option<Vec<String>>,
    pub display_values_supported: Option<Vec<String>>,
    // Valid options are normal, aggregated, and distributed. If omitted, only use normal
    pub claim_types_supported: Option<Vec<String>>,
    pub claims_supported: Option<Vec<Claim>>,
    #[serde(with = "url_serde")]
    pub service_documentation: Option<Url>,
    pub claims_locales_supported: Option<Vec<String>>,
    pub ui_locales_supported: Option<Vec<String>>,
    // default false
    pub claims_parameter_supported: Option<bool>,
    // default false
    pub request_parameter_supported: Option<bool>,
    // default true
    pub request_uri_parameter_supported: Option<bool>,
    // default false
    pub require_request_uri_registration: Option<bool>,
    #[serde(with = "url_serde")]
    pub op_policy_uri: Option<Url>,
    #[serde(with = "url_serde")]
    pub op_tos_uri: Option<Url>,
    // This is a NONSTANDARD extension Google uses that is a part of the Oauth discovery draft
    pub code_challenge_methods_supported: Option<Vec<String>>,
}

#[derive(Deserialize)]
pub enum Claim {
    Name(String),
    FamilyName(String),
    GivenName(String),
    MiddleName(String),
    Nickname(String),
    PreferredUsername(String),
    Profile(
        #[serde(with = "url_serde")]
        Url
    ),
    Picture(
        #[serde(with = "url_serde")]
        Url
    ),
    Website(
        #[serde(with = "url_serde")]
        Url
    ),
    Gender(String),
    Birthdate(String),
    Zoneinfo(String),
    Locale(String),
    UpdatedAt(u64),
    Email(Email),
}

#[derive(Debug, Validate, Deserialize)]
pub struct Email {
    #[validate(email)]
    pub address: String,
}

pub struct Discovered<L: Lifetime> {
    pub config: Config,
    p: PhantomData<L>,
}

impl<L: Lifetime> Discovered<L> {
    pub(crate) fn new(config: Config) -> Self {
        Discovered {
            config,
            p: PhantomData,
        }
    }

    /// Get back the config options this Provider was made with
    pub fn config(&self) -> &Config {
        &self.config
    }
}

impl<L: Lifetime> AuthProv for Discovered<L> {
    type Lifetime = L;
    type Token = Token<L>;
    fn auth_uri(&self) -> &str {
        self.config.authorization_endpoint.as_ref()
    }
    fn token_uri(&self) -> &str {
        self.config.token_endpoint.as_ref()
    }
}

impl<L: Lifetime> Provider for Discovered<L> {
    fn issuer(&self) -> &Url {
        &self.config.issuer
    }
    fn jwks_url(&self) -> &Url {
        &self.config.jwks_uri
    }
}

// Issuer in and out urls should just be https://<domain>:<?port>/<?path>
fn val_issuer_url(url: &Url) -> bool {
    if url.scheme() != "https" || url.query() != None || url.fragment() != None {
        false
    } else {
        true
    }
}

/// Implements [OpenID Connect Issuer Discovery](https://openid.net/specs/openid-connect-discovery-1_0.html#IssuerDiscovery)
///
/// Webfinger is [RFC 7033](https://tools.ietf.org/html/rfc7033)
///
/// This implementation is not generic - Webfingers take a "rel" parameter that is a set of URLs.
/// OpenID Connect only specifies one valid "rel" when querying about Discovery data.
///
/// If a Host is not provided, it is derived from the resource's domain.
///
/// This function returns the OpenID Connect issuer for the given webfinger.
pub fn issuer(
    resource: Uri,
    client: &Client<HttpsConnector>,
    host: Option<Url>,
) -> Result<impl Future<Item = Url, Error = ClientError>, ClientError> {
    let host = match host {
        Some(host) => {
            if !val_issuer_url(&host) {
                Err(ExpectedType(
                    "host must be an https url without a query or fragment",
                ))?
            }
            host
        }
        None => {
            let host = resource.host().ok_or(ExpectedType(
                "resource needs a host without another host provided",
            ))?;
            if let Some(port) = resource.port() {
                Url::parse(&format!("https://{}:{}", host, port))?
            } else {
                Url::parse(&format!("https://{}", host))?
            }
        }
    };

    // Uri should really support into<String>...
    let url = Url::parse_with_params(
        // The OpenID Connect webfinger MUST be retrieved over HTTPS
        format!("{}/.well-known/webfinger", host).as_str(),
        &[
            ("resource", format!("{}", resource)),
            (
                "rel",
                String::from("http://openid.net/specs/connect/1.0/issuer"),
            ),
        ],
    )?;

    Ok(
        client
            .get(Uri::from_str(url.as_str()).map_err(
                |e| hyper::Error::Uri(e),
            )?)
            .map_err(|e| ClientError::from(e))
            .and_then(|resp| {
                resp.body()
                    .concat2()
                    .map_err(|e| ClientError::from(e))
                    .and_then(move |body| {
                        let mut json: Webfinger = from_slice(&body)?;
                        // By openid spec, you should only get one "Link" back
                        let link = json.links.pop().ok_or(ExpectedType("Links can't be empty"))?;
                        if !val_issuer_url(&host) {
                            Err(ExpectedType(
                                "returned issuer is invalid - must be https without query or frag",
                            ))?
                        }
                        Ok(link.href)
                    })
            }),
    )
}

/// Implments [OpenID Connect Configuration Discovery](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfig)
///
/// Returns a Future that will resolve to a Discovery Config document for this issuer.
///
/// Issuer URLs are REQUIRED to be https. Non-https urls will be coerced into https or will error.
#[async]
pub fn config(mut issuer: Url, client: Rc<Client<HttpsConnector>>) -> Result<Config, ClientError> {
    // The issuer must be https by spec. If the wrong schema is used, we force it to https.
    // If you get an InvalidDomainCharacter back you know your url is cannot-be-a-base.
    if issuer.scheme() != "https" {
        issuer.set_scheme("https").map_err(|_| {
            ClientError::Url(url::ParseError::InvalidDomainCharacter)
        })?;
    }
    // This operation CANNOT fail. It is taking a valid issuer and constructing a URI out of it.
    // We just lack good into/from support between hyper::Uri and url::Url
    let uri = Uri::from_str(
        issuer
            .join("/.well-known/openid-configuration")
            .unwrap()
            .as_str(),
    ).unwrap();
    let resp = await!(client.get(uri))?;
    let body = await!(resp.body().concat2())?;
    let config = from_slice(&body)?;
    Ok(config)
}

/// Lifetimes are not known on a per-config basis. All openid providers *should* support
/// expiring tokens, and are a sane and safe default (especially for auth code flow). Only use
/// refresh or static tokens if you know what you are doing!
pub fn provider<L: Lifetime>(config: Config) -> Discovered<L> {
    Discovered::new(config)
}

/// Retrive a Json Web Key Set from a given Uri.
///
/// Since OpenID Discovery and JWK specs do not limit this URL, we don't either.
#[async]
pub fn jwks(uri: Uri, client: Rc<Client<HttpsConnector>>) -> Result<JWKSet<Empty>, ClientError> {
    // See config - this operation CANNOT fail
    let resp = await!(client.get(uri))?;
    let body = await!(resp.body().concat2())?;
    let jwks = from_slice(&body)?;
    Ok(jwks)
}

/// Magical async discovery. client_id, client_secret, and redirect_uri are all your own server
/// settings. If missing you need to get a client id / secret from your oidc provider of choice
/// through other channels. Note that this only works with `Expiring` tokens from servers. If
/// your provider returns refresh or static tokens (as happens in non auth-code flows) you will
/// need to use config() jwks() config() and Client::new() manually.
#[async]
pub fn discover(
    issuer: Url,
    params: Params,
    client: Rc<hyper::Client<HttpsConnector>>,
) -> Result<IdClient<Discovered<Expiring>>, ClientError> {
    let config = await!(config(issuer, Rc::clone(&client)))?;
    // Cannot fail, wish we had into for either of these...
    let uri = Uri::from_str(config.jwks_uri.as_str()).unwrap();
    let jwks = await!(jwks(uri, Rc::clone(&client)))?;
    let provider = provider::<Expiring>(config);
    Ok(IdClient::new(params, provider, jwks, client))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config() {}

    #[test]
    fn provider() {}

    #[test]
    fn jwks() {}

    #[test]
    fn discovery() {}
}
